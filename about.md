---
title: About
lang: en-US
---

# Hi, I’m toastal.

I do web stuff & like to make things pretty. I’m a competent functional
programmer ‘wasting’ my art degree.


#### Things I’m into:

- Green tea (and other caffeine)
- Photography (tools: Fujifilm X-T10, <a href="https://www.darktable.org/" rel="nofollow">darktable</a>, <a href="http://hugin.sourceforge.net/" rel="nofollow">Hugin</a>)
- Languages and linguistics (currently learning Thai and some Lao)
- Programming language features: curried by default, pattern matching, first-class composition infix operators, algebraic data types, managed <abbr title="input/output">IO</abbr>
- Giving technical talks to the local developer community
- Music: math rock, jazz, sadcore, post-rock, second-wave emo, chiptunes, dark synthwave, liquid funk, 60s soul
- Cartoons: King of the Hill, Harmon Quest, Archer, Futurama
- Food (especially with cilantro and garlic)
- <abbr title="GNU’s not Unix">GNU</abbr>/Linux (running Debian & Arch)
- <abbr title="open-source software">OSS</abbr>: <a href="https://www.mozilla.org/firefox/products/" rel="nofollow">Firefox</a>, <a href="https://neovim.io/" rel="nofollow">Neovim</a>
- Hiking
- Puns


#### Things I’m not that into:

- Wearing pants
- Object-oriented progamming
