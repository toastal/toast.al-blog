---
title: Privacy Policy
---

# Privacy Policy

_I_ don’t track your data - not even Google Analytics because I trust my readers are responsibly using adblock and tracking protection anyhow. And I am not using cookies anywhere for you <abbr title="European Union">EU</abbr> cats.

I _do_ use two <abbr title="Content Delivery Network">CDN</abbr>s, Cloudflare’s CDNJS for highlight.js stuff and Google for fonts. Their rules apply to their stuff. Maybe if people [complain](https://twitter.com/toastal) enough, I'll host those things too but I try to use <abbr title="Hypertext Strict Transport Security">HSTS</abbr> and subresource integrity where possible so there’s less chance of any monkey business.
