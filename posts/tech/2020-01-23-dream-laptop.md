---
title: Dreaming of a Laptop
subtitle: Am I too niche?
lang: en-US
tags: laptop, wishlist
description: I can’t find *anything* good enough.
---

Updates later:

I think that it’s reasonable to go with Tiger Lake speficically right, right now. Xe graphics _are_ significantly better than the integrated Ryzen GPUs (which were better than Iris). There would be access to Thunderbolt 4, which means I can buy things that are USB4-compatible and they will work well into the future. Intel is better optimized for Linux for a couple of different reasons, and while this can change, right now it’s not. I’m definitely looking at the HP Spectre x360 14 because of the nice OLED panel and build quality. The things I don’t like about it are: there’s no ability to save money with no OS and 16GB of soldered RAM isn’t ideal (32GB of unsoldered RAM would be my preferred option). What I’m waiting on is Thailand to have the option to order this laptop and I have emailed sales about this (though the last gen wasn’t listed on their site for … who knows?).

- - -

Updates old:

What’s on my eye now is a version of the [Mechrevo Code 01](https://www.notion.so/Mechrevo-Code-01-TongFang-PF5NU1G-Information-8009025fdefc40118ab0ea973e7e0988). There’s supposedly a [4K, 100% DCI-P3 OLED panel](https://www.reddit.com/r/XMG_gg/comments/hjxm80/launch_schenker_via_15_pro_with_ryzen_7_4800h/fy9ksrf/) coming out for the [TUXEDO Pulse](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/TUXEDO-Book-Pulse-15-Gen1.tuxedo#!#h4-configurator) and [Schenker Via 15 Pro](https://bestware.com/en/schenker-via-15-pro.html) using the [Samsung ATNA56WR06 ](https://www.panelook.com/ATNA56WR06-0_Samsung_15.6_OLED_parameter_40394.html). When the basic model out of China comes with an ANSI layout, will I really be able to tolerate these EU options using [ISO over ANSI layouts](https://www.tuxedocomputers.com/en/Infos/Help-Support/Frequently-asked-questions/Keyboards-Layouts_1.tuxedo)? I’m quite set in my ways when it comes to travel distances to my <kbd>Shift</kbd> and <kbd>Enter</kbd> boards and really don’t understand moving the <kbd>\|</kbd> key.

What this checks off (with display upgrade):

- Lightweight @ 1.5 kg (3.3 lbs)
- AMD Ryzen 7 4800H (8x 2.9 - 4.2 GHz Octa-Core, 16 Threads, 8 MB L3-Cache, 45 W TDP)
- Integrated graphics are reasonable (30 FPS on high for Battlefield 5)
- 91.25 Wh battery
- TUXEDO is Linux-supportive, and even with their FAQs state they want [Coreboot support](https://www.tuxedocomputers.com/en/Infos/Help-Support/Frequently-asked-questions/Coreboot-on-TUXEDO-Computers-devices.tuxedo)
- Type A and Type C USB (with power delivery, but NOT with display port)
- Wi-fi 6
- Offers 32+GB RAM (DDR4 3200MHz)
- Can order without an M.2 drive in case I find something else on the cheap (???)

Where it lacks:

- No Thunderbolt (no surprise), and this could allow an eGPU option to make up for the lack of dedicated graphics when needed (never researched Linux support here)
- Stock screen is decent, but not good enough (Innolux N156HCE-EN1)
- Importing from Europe cost more in a lot of ways (and dislike key layouts)
- Mixed reviews on build quality with keyboard flex

So what’s holding me up? Two things.

The first is that this better panel still isn’t out yet. I talked to the Mechrevo store on Thailand’s Lazada but they said they didn’t have this in stock. I could look around some more and probably order and assemble the panel myself, but it’s the ordering of this laptop that is tricky. I wouldn't be surprised if supporting all these keyboard layouts is a part of the upcharges (and I still prefer ANSI using digraphs in Neovim for accented characters).

The second is the Tiger Lake announcement will be out in September. There seems you can trade off some battery efficiency and multi-core performance for better single-core performance and the Xe-powered APU. Additionally since a lot of the major OEMs partner with Intel on design, we don’t see their flagships going AMD so most of the top-of-the-line laptops are (at least this year) all Intel which a better Intel chip really opens up the doors to more selection (and the Thunderbolt thing).

- - -


Currently I’m on a 2016 Razer Blade. My verdict about the machine comes down to it being a good-build-quality machine that has has done me pretty well in over 3.5 years. However it’s starting to age, and batteries, even bought new, don’t hold a good charge. The premium price has paid off mostly since there really hasn’t been as many big jumps in tech since the NVIDA 10xx series of GPUs; I like being on the edge of tech often and not much has really come to actually contend. But with the new AMD 7nm chips, this could be another change of the guard (while Intel goes down to 10nm and is saying their next integrated GPU will have some power). Despite the quality holding up, Razer’s hostility toward people asking for basic Linux support is such a turn off that I would _not_ recommend the brand and I don’t want to repeat the mistake.

<details>

<summary>Specific beef with laptop</summary>

There were BIOS upgrades to fix fans and security vulnerabilities with Intel’s chips. Razer requires Windows 10 + a GUI just to upgrade BIOS. This is dumb. I miss the days where you but a `.bin` file on a USB and loaded it up from the BIOS screen. There’s no good reason not offer this as at _least_ a fallback—the GUI should just exist for normies. _OR_ even being able to upgrade via FreeDOS is infuriating, but workable. I tried using Windows PE to install and that didn't work either because you need Windows 10-Windows 10. I asked support and [they don’t care about supporting <abbr title="Linux Vendor Firmware Service">LVFS</abbr>](https://fwupd.org/lvfs/vendors/#razer) or any other method to upgrade.

I’ve replaced my battery twice already. They start swelling and it warps the machine. They’re like $50-80 a pop.

I’ve had to replace the power supply due to shorts in cables twice.

Not sure who to blame Razer, Nvidia, Linux or myself, but I've never had luck with outputting video via the HDMI port.

Super minorly, the move to <abbr title="Southeast Asia">SEA</abbr> has caused the rubber grips on the bottom have fallen off—and while I quickly recieved some replacements from support, after I alcohol wiped clean the base and stuck the new guys on, they didn’t stay for more than a couple weeks. Humidity?

</details>

I’ve been keeping my finger on the pulse of laptops for over a year now, but it’s been disappointing.


## So what am I looking for

##### Philosophy:

  - Hopefully directly supports Linux or is at least accomodating (like [<abbr title="Linux Vendor Firmware Service">LVFS</abbr>](https://fwupd.org/)).
  - Supports tinkerers and upgrades. I hate the soldered-on part movements or treating laptops as magical black boxes. I’ve mostly ever upgraded <abbr title="random access memory">RAM</abbr>, but it’s the idea.

##### Hard-want specs:

  - Monitor
    - **14" ** - 13" can be a bit small but this standard for 15.6" is larger than I care for and usually comes with machine that is geared towards gaming.
    - **1440p** - 1080p you can see the pixels and 4K is known as overkill for normal viewing
    - **Touch support** - I don't use it to often, but as convertable start to rise in popularity, having a stylus would be _amazing_. Maybe I could get back into art.
    - **Wide color gamut** - I do color photography so I want at _least_ 95% coverage of either Adobe RGB or DCI-P3 but a lot of manufactures stop at sRGB. It’s fairly standard to have DCI-P3 on good smart phones, so why would I expect less from my laptop? Especially with <15" screens, you rarely see the color support.
    - **OLED** - there are some color accuracy issues shifting brightness, but I can calibrate with my ColorMunki and always edit at a specific brightness (which is how I do it already). I think the deeper blacks could change my style even. But also, these panels consume less power when the screen is black.
  - Keyboard (area)
    - **NO number pad** - I do not use it and it causes the whole machine to sit off balance in a lap (laptops go in laps??). To get smaller-bezel machines, manufacturers have axed the speakers and throw them under the machine or under the keyboard when there’s plenty of stereo space for good speakers.
    - **Speakers don’t suck** - They don’t have to be amazing, but often built-in speaker sound tinny with no bass (which is helped by proper placement because of no number pad). I have nice headphones/earphones, but I don’t always want to use them.
    - **Trackpad doesn’t suck** - People act like Apple has some monopoly on this market, and they are wrong; however, if the trackpad is bad, then it’s not really worth considering.
- Hardware
  - **AMD Ryzen** - I’d like to support Team Red, the price/preformance/battery hit all the right spots but only gaming laptops have shown a big interest in AMD. There seems to be less security issues too.
  - **GREAT APU** - Given my workload (code, photos, light gaming), I think in 2020, we can probably have a good enough integrated graphics to ditch the extra moving parts, heatsinks, and toll on the battery life.
  - **6+ cores** - It’s 2020 and I have some workloads that could use it.
- Ports
  - **USB4** - Intel’s allows for Thundlebolt 3 to be licensed to others (like Apple’s new ARM-based chips), but there are bascially no non-Intel options out there. Sadly it looks like there won’t be any USB4 til 2022.
  - **1+ USB3** - Too many laptops are following Apple’s poor lead, as dumb as phones are without 1/8" headphone jacks, not having at least 1 USB type A port is naïve and cumbersome. Dongles are fine, but demanding them is adsurd. I have a USB token key and to have to pull at the dongle for this is not desireable.
  - **1/8" headphone jack** - see above
- Battery
  - **Lasts 8+ hours** - under regular use that I have with coding.
- Physical
  - **<4 <abbr title="pounds">lbs.</abbr>** - I carry this many places. My 4.1 <abbr title="pound">lb.</abbr> laptop is heavy enough as is, but it has a decidicated GPU.

##### Nice-to-have specs
  - Monitor
    - **75–120hz refresh rate** - It definitely looks and feels smoother, but isn't necessary at all. Is this something easy to tie into `tlp` and battery?
    - **Better than 16:9 ratio** - Extra space is nice.
  - Hardware
    - **>16GB RAM** - I have had 16GB of RAM in this 3.5-year-old laptop already. Since I don’t run a Chromium-based browser, the only time I really run into this as a limit is when I’m editing photos, but that’s important to me.
  - Ports
    - **SD card** - This would help with photo stuff, but I can use USB anyhow (but will I need a dongle to get USB to my laptop because of dumb aesthetic choices).
    - **Ethernet** - I rarely use it, but it just seems every time I _do_ need ethernet, it seems like it’s absolutely necessary.
  - Firmware
    - **Coreboot** - This is a lot to expect, but open source UEFI stuff for security and performance and ease of updates would be great

Put these specs in Amazon or Newegg and you get no results. :(

Let’s hope this changes.
