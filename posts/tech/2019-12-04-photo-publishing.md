---
title: The State of Social Photo Publishing Rounding Out 2019
subtitle: Why Instagram is Trash & Why the Fediverse is Good
lang: en-US
tags: photography, flickr, pixelfed, fediverse
description: It’s a mess
---

TL; DR: I need to make an effort to help the Fediverse to get the features I want in a photo platform because they’re all flawed.

[![Late Chill Stroll at Wat Rong Khunimg](https://live.staticflickr.com/65535/49163270231_bfcd457957_b.jpg){width=745 height=1024 loading="lazy"}](https://www.flickr.com/photos/toastal/49163270231 "Late Chill Stroll at Wat Rong Khun")

I like photography. I also like when others can view my photos and interact with a community (but of the photographers and photography enthusiasts). Right now there are options and a few big players in that space.

I’m dismissing immediately Imgur and Tumblr because while they are social networks/communities, they’re aimed for a different community than my intent—more of the meme-share-y kinda thing. Anything run by Google or Amazon should probably be a no (as is Facebook, but that one’s trickier in this case). There’s also the more hosting-solution-oriented options like SmugMug or Unsplash (which only has one license), et. al., but they don’t really offer feedback, trending, sharing type of thing—it’s just a way to display and get credit for your work.

So really the only ones that seem to have groups or contest or sharability with a professional/serious amateur vibe that fit the general idea I'm looking whittles down to:


- Instagram
- Flickr
- 500px
- DeviantArt
- ~~Google Photos~~
- ~~Amazon Photos~~
- ~~Facebook, in general~~
- ~~Unsplash~~
- ~~Tumblr~~
- ~~Imgur~~
- ~~SmugMug~~


Many years ago I used DeviantArt, but it’s faded in some ways as a photography thing and when I did more drawing/graphics, it made more sense. I went to school for art, but I don’t enjoy designing like I did as you can tell by this half-baked site as of this post. 500px vs Flickr has been a long ‘rivalry’, where it seems their both quite similar in letting you license photos, find groups of similar taste, participate in contests. Having a Flickr for so long, I never saw a big enough reason to switch other than hoping to trying to get some sort of monetary kick-back as we’d all love some free cash (although my style of street photography doesn’t often lend itself to the stock photography style). It seems though that 500px has a strong preference for a particular style of photography and is filled with bots. Also the 7-photo per week thing is curse for me. It does ensure you’re selective about your uploads, but I tend to upload in bursts after a event and go dormant for weeks or even months. You can pay to circumvent this, but what else are you getting?

Flickr however, especially since the 1TB limit of yesteryear, became a dumping ground for a lot of normal people just wanting a back-up which added a lot of noise.

I’ve still been using Flickr as my primary posting platform relinking everything back to that Flickr account (and now post SmugMug acquisition offer the ability to change your account ID, which would have been better than creating a new account).

[![Inside Wat Luang](https://live.staticflickr.com/65535/48255109407_7b4fdf696c_c.jpg){width=800 height=309 loading=lazy}](https://www.flickr.com/photos/toastal/48255109407/ "Inside Wat Luang")


## So why is Instagram on this list?

Instagram is currently the leader in a lot of spaces around getting your art/content followed and like and appreciated. It’s the best marketing tool to many. It has so many flaws though. You see it used by everyone in this ubiquity sense from people’s lunch and pets, to photographers, to digital artists, to fashion icons, to memes, and so on.

Instagram is also (arguably) ruining society right now causing FOMO, depression in wanting to live up to others’ lifestyles, and it’s destroying natural places for getting photo shoots. We used to not have this sort of issue and while some platform was going to have to bear the weight of this societal issue with social media, Instagram is the worst of it now.

It’s also filled with bots, and has weird gated content with the follow requirements. Users end up going private due to harrassment and whatnot.

So why do I prefer Flickr to Instagram:

1. Flickr doesn’t strip your metadata… Which includes all that juicy EXIF camera data about everything that happened in the shot and the gear used, embedded color profiles if you use a color calibrated workspace (like I do) so everyone can see your image exactly as you intended, and your license. This can all be searched too!
2. Flickr doesn’t convert to sRGB so you can have wider color gamut. A lot of phones now support DCI-P3 (and newer OLED laptops) and high-end monitors often support AdobeRGB. I believe the iPhone app will allow DCI-P3 specifically, but that’s a small slice of the support pie.
3. While there are limits, you can upload strange dimensions where Instagram limits you basically to a square so panoramas are impossible. People may use letter-boxing in some cases, or making the seams match up with multi-photo post, but these are _hacks_ and you can’t do anything about most portrait shots (odd seeing how they drive you hard to use the smartphone app, but you can't upload an uncropped photo straight from the camera).
4. Flickr keeps your original uploaded file so you have a backup in case of an unforeseen event or some offhand need to get a copy now.
5. Flickr lets me edit my content even after posting; not just the title and comment, but the license, location, and even the image itself can be reuploaded if you need to make a tweak.
6. Flickr is still a hot spot for finding Creative Commons licensed content (which almost all of mine is), so the licensing is clear, but also people looking to remix or use on their blogs can find content that works for them.
7. Flickr is not owned by Facebook, and while it’s not open and is connected to an ad network like many sites, it isn’t directly feeding into that big blue machine.
8. Not being Instagram, Flickr doesn’t have a bunch of normie uploads. I don't like to gate-keep photography, but it's nice to not have to wade through coffee cup photos or follow specific people to see what I want. There absolutely is a place for that style of thing, but I want these separated.
9. …because of #8, browsing the daily interesting photos are pretty good (outside the Second Life content that always slips in). The interestingness algorithm is guarded, but they do have a system that helps good photos be found although I would like to know more.

I don’t think these things can be overlooked in the sort of experience I want. If you haven’t noticed, all my images in this post I had issues uploaded to Instagram for a number of reasons like dimensions and colors and cropping to a square on previews.

What Instagram and its ubiquity gives you despite all of these is the ability to have anyone heart and comment with your content. I can’t get regular people to sign up to Flickr to follow my stuff and too many people don’t know how RSS works, and even still, they can’t interact.

[![Menora on One Leg](https://live.staticflickr.com/65535/48084010803_2d8ecd7cd7_c.jpg){width=800 height=534 loading=lazy}](https://www.flickr.com/photos/toastal/48084010803/ "Menora on One Leg")


## So then what’s wrong with Flickr?

While the SmugMug acquisition did breathe new life into it, Flickr now has a 1000 photo limit for free members. If I thought the paid service were worth it, I’d consider. The biggest feature really is the unlimited storage and backup and I can get that elsewhere. The rest is capitalist bullshit deals to try to get you to buy more stuff you don’t need. I particularly don’t like people partnering with Adobe as its subscription model was a toxic move for that sort of software which, along with switching to Linux, swiftly prompted me move to open source solutions. Switching from Lightroom to darktable was a breeze and it’s powerful enough that I rarely need to reach outsidfe o it (which would be even more true, if Fuji support were a bit stronger so panoramas could be done in-app, but I didn't know what I was [getting into with X-Trans cameras](https://www.darktable.org/2014/08/using-x-trans-cameras-with-darktable/)).

The community—it exists, but many groups are mere husks of their former glory and have new new threads in months. Newcomers are rarer and it seems many just stick to Instagram. I *do* enjoy the [Southeast Asia group](https://www.flickr.com/groups/southeastasiaflickr/) currently as it’s active and has monthly contents for amateurs.

However, it’s still owned by a corporation. I get to keep my licenses which is nice, but with a dwindling community, and knowing a closed source business is behind all my photos is unsettling. I could host myself and it’d be similarly priced, but I wouldn’t get the

So for now it’s working, but I’m unsure of the future.

[![Spouting Rock Higher Ground](https://live.staticflickr.com/4379/36757474932_bd4a912ab7_h.jpg){width=890 height=1600 loading=lazy}](https://www.flickr.com/photos/toastal/36757474932/in/datetaken/ "Spouting Rock Higher Ground")

## What else could I want?

Well, how about I keep good licenses but also my files. I want to host it myself, but also be a part of a network—a network that doesn’t require anyone signing into one particular service. This is what the Fediverse gives you. It’s decentralized, and you can use basically any service to like or boost the original post from any other service (think getting to ‘reweet’ a Bandcamp song using Mastodon + Funkwhale).

What are the current options in this space though? Well, with [PeerPx being on hiatus](https://github.com/peerpx/peerpx/issues/19#issuecomment-476573898) because it’s a lot of work to make this sort of thing. You have to either make your own (which could be fun if I liked back-end programming, but I don’t) or use the closed-to-polished [Pixelfed](https://pixelfed.org/).

Pixelfed is good, don’t get me wrong, but it does lack in the sort of features I’d want in a platform. Being open source though, I have the ability to help. I’ve so far submitted a few bugs or comments, but haven’t worked on any code (though I have read some). The initial knee-jerk was that I hate the stack choice—which you’d normally never see with a closed-source dealio.

<details>
<summary>Biased rant against Pixelfed’s stack</summary>
<div style="background-color:var(--color-code-bg,rgba(0,0,0,0.08));padding:0 1em">
I despise PHP after having done a number of WordPress and Drupal things early on in my career and it’s not at all functional-programming-friendly. I can forgive this though in the sense that if you want to maximize the ability for people to run their own instances, PHP and its shared hosting situation making it a goto. Granted, VPSs are nearly the same price but whatever. A modern application tends to be front-end-heavy anyhow so the back-end isn't all the relevant especially if you start using things like GraphQL and the like. It doesn’t do this and is a bunch of encapsulated Laravel, but 🤷. I mean I guess it can work offline if it’s not front-end heavy, but this is definitely an app.

Had it been a more traditional SPA, different, competing front-ends could be made to consume the content.

The front-end however is Vue, which I dislike even more. It’s like the the ‘magic’ of Rails meets the mess that is Angular. Directives on HTML are just bad. Data flow through stateless components is so much easier to reason about. If you use their templates you have the same issues you do with JSX’s magic without improving on the XML syntax which is harder to read and easy to make mistakes with the main selling point being _fAmiALaRiTy_. I took a brief foray in it testing the Vue waters for a remote gig and was basically done with it after a week and literally done after two. If you have to do vanilla JS/TS, you should be probably be in React—and still I’m biased towards FP stuff. (Although Svelte a couple releases ago actually offers something worth looking at).

I don’t even like SCSS-style Sass. It adds a compile step with a parser but SCSS doesn’t improve on the ugliness of CSS (who even likes semicolons?). I’ve switched to SugarSS on basically every project and haven’t looked back.

The stack reminds me of the advertising days, where the goal was to have the most common-denominator, milquetoast stack possible to be able to pull off random people from the street to be able to touch it. It’s so _normal_ it kinda actually disgusts me. Maybe that’s a problem with me…
</div>
</details>

My distaste to contribute to a project with this stack aside, dansup built all this and it works and I’m merely ranting from a sideline so I can probably shut up about this.

Pixelfed does feature federation so you can talk via anything using ActivityPub under the covers. It also is big on NSWF tags, CW tags, and licenses which are great.

##### It _is_ still missing features however. Notably to me:

- Editing: Mastodon has “delete & redraft”, which is better than the nothing this has, but I find myself needing to adjust colors once it’s uploaded and check on my phone too or I made a typo or I want to add some tags I forgot. Sadly maybe this is [impossible with the Fediverse though](https://github.com/tootsuite/mastodon/issues/256) (though the append only and `git`-style edits were intriguing). I think image uploading behaves different than a ‘tweet’ does. I guess even the ability to do a preview state would help, but I totally hate being stuck with errors or having to redraft and losing my faves/boosts just to change the license. It seems [Plume, federated blogging](https://github.com/Plume-org/Plume) offers the ability to edit a post. I think though that editting the actual image’s EXIF data is the true goal here and it should be kept in sync.
- Color profile and metadata stripping: I get that we want to make assets nice and small for web transport, but this images need licenses and stuff. And if you want a platform to be a serious competitor for photography, you have to keep the [color profiles](https://github.com/pixelfed/pixelfed/issues/1797). Since I’m in favor of keeping the image data on the image, this obviously needs to stay, regardless of if it’s redundant.
- [Save the original option on the server](https://github.com/pixelfed/pixelfed/issues/1848): It can be implemented many different ways, but having an access to a backup in case of emergency or needing to migrate is important.
- Show EXIF data. I want to see the cameras used. I want to see some badging bonus for people using open source editing software like darktable or RAWTherapee.

##### Nice to haves:

- A Flickr-like feed where things aren’t limited to squares but can show the complete image albeit scaled down.
- …if not, give the user the [ability to set the `object-position`/`background-position`](https://github.com/pixelfed/pixelfed/issues/1870) when these photos get cropped for such view. I have Images where the subject is cut because I build the composition around such weight. It also would not be that hard as there are 9 possibilities between, `top / center / bottom` and `left / center / right` (having a 9 points on a box to click along with an image thumbnail preview to make it more obvious). Or number/ranger sliders for x/y percentages that can also be used by dragging a thumbnail to a specific spot. Or heck, fully expose it allow `calc()` (though sanitization would be required). The discover page would have more intent this way.
- Scale down images that are too big in the feed rather than stretching images that are too small. It looks terrible.
  <details>
  <summary>Solution?</summary>
  ```
  /* and similar */
  .card-img-top {
    display: block;
    object-fit: scale-down;
    width: auto;
    max-width: 100%;
    margin: auto;
  }
  ```
  </details>
- Let’s see some other image sizes for pixel density. And maybe using alternatives file formats like webp (or FLIF via WASM if feeling cheeky).
- Read some of these fields from the EXIF data like title, description, and tags.
- Tag/hashtags overhaul where they’re not a part of the description. It muddies the text to see <a>#me</a> do <a>#this</a> sort <a>#ofthing</a>. If tags are separate, the description would be more legible. Append the tags as hashtags at the end of the description for the greater Fediverse though.
- Groups?: I mean you could create a collective on a instance or just follow the right people… Although this would likely be better solved using a different ActivityPub platform and associating the Pixelfed account.

[![He Rings the Bell](https://live.staticflickr.com/1886/44274421271_ee7dcb5a68_b.jpg){width=1024 height=684 loading=lazy}](https://www.flickr.com/photos/toastal/44274421271/in/datetaken/ "He Rings the Bell")


## What to do now?

Well, I should probably suck it up and contribute to Pixelfed instead of gripe (which would be a good New Year’s Resolution in contributing to open source). It seems the changes I want would likely be flags for the admin or env which means touching back-end code _AND_ PHP. Unless someone out there has an itch to write a back-end for a Flickr alternative?

With some comments about [Pixelfed’s paradigm](https://github.com/pixelfed/pixelfed/issues/1099#issuecomment-482311887), maybe something different is actually more warranted. It seems there is a regular struggle between those that envision a photo enthusiest UI and those that do not. The main issue is that Pixelfed is by far the most polished and ready to use now.
