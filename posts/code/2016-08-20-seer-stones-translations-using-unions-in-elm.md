---
title: Seer Stones
subtitle: Translations Using Unions in Elm
tags: elm, i18n
description: Solving internationalization in Elm
elm-hl: True
---

In many languages, to do translations, a team would pull in a library to handle some of the basic internationalization (<abbr title="internationalization">i18n</abbr>). In Elm though, we have access to tools to do this on the language level without a library and instead can use a couple of functions and unions—lessening our dependencies.

_So what’s so awesome about using union types?_ Unlike a `Dict`, we get type safety for the translations to verify that they exist and we’ve covered all the cases. It’ll be very easy to reason about the translations since everything is just a couple of `case` statements. It will look a bit fluffy, but that’s `elm-format` for you.


- - -


The accompanying source code for this post is [in this GitHub repository](https://github.com/toastal/elm-translation-example).
There’s also a [demo](#elm-translation-demo)—so that’s kinda neat.


- - -


## So What Are the Pieces to the Translation Puzzle?


#### `Langauage`

Our `Language` is a union of the languages the application supports.

```elm
type Language
    = EnUk
    | EnUs
    | EsMx
```


#### `Phrase`

These are the basic building blocks and represent the phrases that the application will be translating.

```elm
type Phrase
    = Greeting String
    | Name
    | TextColor String
    | Color
    | Red
    | Blue
    | Green
```


#### `Translator`

An alias for function from our phrase to a string to display.


```elm
type alias Translator =
    Phrase -> String
```


#### `translate`

Part 1: A top-level function that can be partially applied with our language at the `view` level that selects the appropriate translate function for the rest of the application.

```elm
translate : Language -> Translator
translate lang =
    case lang of
        EnUk ->
            EnUk.translate

        EnUs ->
            EnUs.translate

        EsMx ->
            EsMx.translate
```


Part2: Breaking down the top-level function into sub-level translations will make this easier to digest along with giving us a hand-off-able file for translators to do their thing.


```elm
translate : Phrase -> String
translate phrase =
    case phrase of
        Greeting name ->
            "Yo, " ++ name ++ "!"

        Name ->
            "Name"

        TextColor color ->
            "The color of this text is " ++ color ++ "."

        Color ->
            "Color"

        Red ->
            "red"

        Blue ->
            "blue"

        Green ->
            "green"
```


So between these 4.5 concepts we can wield an entire way to translate across our application. In this example an `I18n`  folder with a structure looks like this:


```tree
├── Languages
│   ├── EnUk.elm
│   ├── EnUs.elm
│   └── EsMx.elm
├── I18n.elm
└── Phrases.elm
```

…where `Phrases.elm` contains our `Phrase` union, `Languages/*.elm` contains just the `translate` function for its corresponding language, and `I18n.elm` contains the rest of the `Translator`, `Language` union, and the top-level `translate`.


- - -


## Hold Up: There’s Another Thing We’ll Need Though

And that is a way to get from a string sent to our app to the `Language` union. I’ll be using this guy since it fits my needs of peeling out [`navigator.language`{.js}](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorLanguage/language) from the browser (i.e. a string like `en-US` or `es_MX`). We would traverse the array from [`navigator.languages`{.js} if support was better](https://developer.mozilla.org/en-US/docs/Web/API/NavigatorLanguage/languages#Browser_compatibility):

```elm
import Regex
import String


toLanguage : String -> Language
toLanguage lang =
    let
        -- will split our string on non-chars, take the first
        -- 2 matches, and lowercase them
        codeFinder : String -> List String
        codeFinder =
            List.map String.toLower
                << List.take 2
                << Regex.split (Regex.AtMost 2) (Regex.regex "[^A-Za-z]")

        -- pattern that regex into Tuple2 of Maybes
        -- containing the ( language code, country code )
        locale : ( Maybe String, Maybe String )
        locale =
            case codeFinder lang of
                a :: b :: _ ->
                    ( Just a, Just b )

                a :: _ ->
                    ( Just a, Nothing )

                _ ->
                    ( Nothing, Nothing )
    in
        -- Using pattern matching and wildcards, we can
        -- choose the appropriate language and fallbacks
        case locale of
            ( Just "en", Just "uk" ) ->
                EnUk

            ( Just "en", Just "au" ) ->
                EnUk

            ( Just "en", Just "nz" ) ->
                EnUk

            ( Just "en", _ ) ->
                EnUs

            ( Just "es", _ ) ->
                EsMx

            _ ->
                EnUs
```


- - -


## So What’s the Most Basic Example of Putting This All Together?


```elm
import Html exposing (text)
import I18n.I18n as I18n exposing (Translator)
import I18n.Phrases as Phrases


main : Html String
main =
    let
        -- in an app, we’d build a `translate` function
        -- here and hand it around to our views so they
        -- can reference it
        translate : Translator
        translate =
            I18n.translate <| I18n.toLanguage "en-US"
    in
        -- in composing `text` and `translate` we have
        -- `Phrase -> Html String`
        text << translate <| Phrases.Greeting "toastal"

-- displays: Yo, toastal!
```

## But Let’s Get “Fancy” With an Example

###### (Those are air quotes)

[_☞ See the source here_](https://github.com/toastal/elm-translation-example)


- - -


<div id="elm-translation-demo" style="overflow-x:auto">Loading…<noscript>The demo requires JavaScript</noscript></div>
<script type="application/javascript" src="/js/demos/elm-translation-demo.js"></script>
<script>
var demo = document.getElementById("elm-translation-demo");
demo.innerHTML = "";
Elm.Main.embed(demo, {language: navigator.language || "" });
</script>


- - -


## Takeaway


Using a couple language-level features in Elm—unions, cases, and functions—we can create a seer stone for our app to translate all of our custom phrases. It’s hardly complicated enough to require a library for a simple use case. One thing to keep in mind though is depending on your translation service, you may have to create some parser/Elm code generator to move from another format that’s not a `.elm` file if required, but that shouldn’t be terribly complicated.
