---
title: Elm Applicatives & Json Decoders
subtitle: Mapping & Applying Our Way to Deserialization Victory
lang: en-US
tags: elm, applicative, json, json decoder
description: Elm decoder as an applicative functor teaching how this concept works as a real abstraction rather than a one-off DSL
shell-hl: True
elm-hl: True
---

_Originally for Elm v0.17. Updated on for Elm v0.18._

One of the first things you will notice is [`Json.Decode.map`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#map) and [`Json.Decode.map8`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#map8), and the numbers between. Soon you will ask: how do decode structures larger than [`map8`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#map8)—where is `map9`{.elm}, `map10`{.elm}, and, more importantly, `mapN`{.elm}? The documentation will lead you to [elm-decode-pipeline](https://package.elm-lang.org/packages/NoRedInk/elm-decode-pipeline/latest) which is not inherently bad, but hides the underlying concept from users with a [<abbr title="domain-specific language">DSL</abbr>](https://en.wikipedia.org/wiki/Domain-specific_language).

In Elm, [`Json.Decode.Decoder`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#Decoder) is an [*applicative functor*](https://en.wikipedia.org/wiki/Applicative_functor)—a fancy functional programming term that we’ll attempt to demystify. By leveraging the power of `map`{.elm} and `apply`{.elm}, we should be able to wrangle even the toughest <abbr title="JavaScript Object Notation">JSON</abbr> thrown our way. I’ll be the first to admit that initially I found <abbr title="JavaScript Object Notation">JSON</abbr> decoding incredibly confusing—literally to the point where I abandoned some projects early on because I didn’t know how to decode some scary <abbr title="JavaScript Object Notation">JSON</abbr>.

### What we’re building towards:

[Pokémon Viewer working demo](https://codepen.io/toastal/pen/kXAKPk)


- - -


#### If you want to follow along and run the code for yourself

This install Elm, its dependencies, and start up Elm Reactor for you to point your browser at `http://localhost:8000`.

```sh
git clone https://github.com/toastal/elm-applicatives-and-json-decoders.git
cd elm-applicatives-and-the-json-decoders
npm install
npm start
```

- - -


## Quick Fly-By at Applicatives via `Maybe`{.elm}

So even the most novice Elm developer knows how `Maybe`{.elm} works—it’s a `Just a`{.elm} or `Nothing`{.elm}.

To go from a `Just 1`{.elm} to a `Just 3`{.elm} we'd use `map` because `Maybe` is a [*functor*](https://en.wikipedia.org/wiki/Functor).


```elm
Maybe.map ((+) 2) (Just 1) == Just 3
--=> True
```


The `a` in `Just a` can also be a function.

So what happens if we had a `Just (+)`{.elm} with the addition infix operator… how do we use this to add in an applicative manner to add `Just 1`{.elm} and `Just 2`{.elm}?


### Applicatives have 2 terms:

- `pure`, `singleton`

- `apply`, `ap`, `<*>`


## Pure

`pure`{.hs} is a function from something `a` that creates a singleton list for the default case of an applicative of the same type `a` (which is why it sometimes goes by the name `singleton`). But to make it more clear, let’s look at the type signature of `pure` in Haskell.


```haskell
pure :: Applicative f => a -> f a
```

Knowing that, let’s look at some examples of Elm singletons:


#### Maybe

```elm
Just : a -> Maybe a
```


#### Result

```elm
Ok : a -> Result x a
```


#### Set

```elm
singleton : comparable -> Set comparable
```


#### List

```elm
flip (::) [] : a -> List a
```


## Apply

Next we peek at the ability to lift values in an applicative with
apply (Haskell):


```haskell
liftA :: Applicative f => (a -> b) -> f a -> f b
(<*>) :: f (a -> b) -> f a -> f b
```


So what is exactly is `Just (+)`?


```elm
foo : Maybe (number -> number -> number)
foo =
    Just (+)
```

Looking at the type signature, it’s a `Maybe`{.elm} holding the addition
function, `(+)`{.elm}.


What is [`Maybe.Extra.andMap`](https://package.elm-lang.org/packages/elm-community/maybe-extra/3.0.0/Maybe-Extra#andMap)?


```elm
andMap : Maybe (a -> b) -> Maybe a -> Maybe b
```


That looks an awful lot like apply/lift… So let’s use it:


```elm
import Maybe.Extra as Maybe

-- foo = Just (+)

bar : Maybe (number -> number)
bar =
    foo |> Maybe.andMap Just 1
```


And let’s apply values to completion


```elm
baz : Maybe number
baz =
    bar |> Maybe.andMap Just 2


isJust3 : Bool
isJust3 =
    baz == Just 3
--=> True
```

And the same thing, creating an infix for andMap/apply:

```elm
import Maybe.Extra as Maybe


singleton : a -> Maybe a
singleton =
    Just


infixl 2 <*>
(<*>) : Maybe (a -> b) -> Maybe a -> Maybe b
(<*>) =
    flip Maybe.andMap


isJust3 : Bool
isJust3 =
    (singleton (+) <*> Just 1 <*> Just 2) == Just 3
--=> True


isNothing : Bool
isNothing =
    (singleton (+) <*> Just 1 <*> Nothing) == Nothing
--=> True


isAlsoNothing : Bool
isAlsoNothing =
    (singleton (+) <*> Nothing <*> Just 2) == Nothing
```


Look at the the demo [`MaybeApplicative.elm`](https://github.com/toastal/elm-applicatives-and-json-decoders/blob/master/demo/MaybeApplicative.elm).


## So where have we seen something like this?


```elm
-- given the function foo…
foo : number -> number -> number
foo x y =
    x * y


-- partially apply in a 1
foo' : number -> number
foo' =
    foo 1


-- Easter Egg: we’ve created a monoid
foo' 37 == 37
--=> True
```


The <code> </code> (space) operator is *function application* ;)


- - -


## So how does this relate to Json Decoders?


Looking in the docs for [`Json.Decode`](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode) we have [`succeed`](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#succeed):

```elm
succeed : a -> Decoder a
```

Looks pretty pure and singleton-y to me…


And in [`Json.Decode.Extra`](https://package.elm-lang.org/packages/elm-community/json-extra/2.0.0/Json-Decode-Extra) we have [`andMap`](https://package.elm-lang.org/packages/elm-community/json-extra/2.0.0/Json-Decode-Extra#andMap) and its flipped infix [`|:`](https://package.elm-lang.org/packages/elm-community/json-extra/2.0.0/Json-Decode-Extra#|:)


```elm
andMap : Decoder a -> Decoder (a -> b) -> Decoder b
(|:) : Decoder (a -> b) -> Decoder a -> Decoder b
```


Well that’s obviously the apply function…


## So let’s apply (heh heh) our knowledge


We’ll start by creating some hand-rolled, artisnal <abbr title="JavaScript object notation">JSON</abbr>:

```elm
coolJson : String
coolJson =
    """
    [
        {
            "foo": 0,
            "bar": true
        },
        {
            "foo": 1,
            "bar": true
        },
        {
            "foo": 2,
            "bar": false
        }
    ]
    """
```


Create some type alias to represent these cool data items


```elm
type alias CoolItem =
    { foo : Int
    , bar : Bool
    }
```


We are also going to need to know about [`field`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#field) (whose old infix was `:=` prior to Elm v0.18) with the signature `String -> Decoder a -> Decoder a`{.elm}

So [`field`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#field) is used to apply the given decoder given a string for a key in a JSON object (e.g. "foo" will be decoded as an integer).


```elm
import Json.Decode as Decode


fooDecoder : Decoder Int
fooDecoder =
    Decode.field "foo" Decode.int
```


## Now that we’re set up, let’s create a `CoolItem` decoder using applicative-style


A reminder about Elm types, `CoolItem`{.elm} can be used as a constructor for our `CoolItem`{.elm}, which is this case is `CoolItem : (Int -> Bool -> CoolItem)`{.elm}.

So what happens when when apply in our foo decoder?

Let’s look at some type signatures and find out:


```elm
import Json.Decode as Decode exposing (Decoder)
import Json.Docode.Extra as Decode exposing ((|:))


-- Creating out Decoder our CoolItem constructor
baz : Decoder (Int -> Bool -> CoolItem)
baz =
    Decode.succeed CoolItem


-- When we apply a decoder for our first property, foo => Int
qux : Decoder (Bool -> CoolItem)
qux =
    Decode.succeed CoolItem
        |: Decode.field "foo" Decode.int


-- And the full decoder, which completes the CoolItem constructor
coolItemDecoder : Decoder CoolItem
coolItemDecoder =
    Decode.succeed CoolItem
        |: Decode.field "foo" Decode.int
        |: Decode.field "bar" Decode.bool
```


So now that we have a `Decoder CoolItem`{.elm}, all we have to do now is set up our app to decode the actual JSON string into a `List CoolItem`{.elm}.


```elm
import Html exposing (Html, text)
import Json.Decode as Decode


view : a -> Html String
view =
    text << toString


main : Html String
main =
    coolJson
        |> Decode.decodeString (Decode.list coolListDecoder)
        |> view
```


`Decode.decodeString : Decoder a -> String -> Result String a`{.elm}

`Decode.list : Decoder a -> Decoder (List a)`{.elm}


But, go look at the demo [`JsonDecodeApplicative.elm`](https://github.com/toastal/elm-applicatives-and-json-decoders/blob/master/demo/JsonDecodeApplicative.elm).


- - -


## Thinking About Elm Applicatives

So how do we find Applicatives in a language like Elm without type classes? Look for type signatures, common names, or think about what the `singleton`{.elm} and `andMap`{.elm} would be.

In Elm you’ll see the term `singleton`{.elm} or `succeed`{.elm} (like `Decoder`{.elm} and `Task`{.elm}) for `pure`{.hs}. …And most of the time you'll see `andMap`{.elm}, for `<*>`{.hs}.

**So let’s see some in action** which uses `Decode.map`{.elm} and
nested `(|:)`{.elm} with some real <abbr title="JavaScript Object Notation">JSON</abbr> <abbr title="Hypertext Transfer Protocol">HTTP</abbr> requests because some folks want to see a more real-world example about how to put this together with `Task`{.elm} and `Cmd`{.elm} well: [Pokémon Viewer demo](https://codepen.io/toastal/pen/kXAKPk) [`PokemonViewer.elm`](https://github.com/toastal/elm-applicatives-and-json-decoders/blob/master/demo/PokemonViewer.elm).


- - -


## Takeaway

If you’re just looking into the base [`Json.Decode`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode) that comes in the core, you’ll see that `Json.Decode.mapN`{.elm} stops at [`map8`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#map8). Many people dead-end here and don’t know where to look next when their objects are bigger. Other blog articles you might find online will often show you how this can be done with `elm-decode-pipeline` or something else, but they are showing you *how* instead of *why*. The *why* for why this works is applicatives—which is based in math and laws. With the applicative style, we have a tool that can cover all cases as well as an understand about what’s going on under Elm’s hood. An important caveat though, is that these will fail silently when you write bad decoders. This can be solved using [`Result`{.elm}](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Result)s and the [Run A Decoder](https://package.elm-lang.org/packages/elm-lang/core/5.0.0/Json-Decode#run-decoders) of the documentation—or using the `mapN` functions when possible.


- - -


Special thanks to [fresheyeball](https://twitter.com/fresheyeball) for
explaining this shit to me.
