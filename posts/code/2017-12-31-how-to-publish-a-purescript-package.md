---
title: How To Publish a PureScript Package (2017)
subtitle: I Wanna Be Pursuit Famous!
tags: purescript, pulp, pursuit, packaging
description: What you need to do to publish a package or library to Bower and Pursuit
haskell-hl: true
shell-hl: true
---

_Update 2020:_ This is out of date as you _should_ be using [`spago`](https://github.com/spacchetti/spago) and creating a `spago.dhall` file, but `pulp publish` is still the way to get things into Pursuit. As `bower` is basically dead, I’m unsure what the future is for Pursuit with this.

- - -


I did a quick search engine run for “publish purescript package” and got no results. I asked a friend, but you may not have that sort of reference at your fingertips. This isn’t meant to be long-winded, but a brief checklist to get you there.


- - -


## Requirements

- `bower`
- `pulp`


```sh
yarn add global bower pulp
# or
npm install -g bower pulp
```


- - -


## The Must-Haves

- Pick a licence and add a LICENCE file to your repo and in your `bower.json`
- dependencies and devDependencies are in `bower.json`
- README
- Documentation comments
- It should at least build via `pulp build -- --censor-lib --strict`


## The Great-To-Haves

- Tests in `/test/`
- Examples in `/examples/`


- - -


## Ready to Publish?

Push a new version tag like this.

```sh
pulp version
```

outputting something similar to

```sh
* Checking your package using purs publish...
Dry run completed, no errors.
* The current version is v1.0.0
You can bump the version to:
a) v2.0.0
b) v1.1.0
c) v1.0.1
Choose one, or enter a specific version:
```

Follow those instructions to get your tag.

Use `pulp` to publish which will grab the latest version from the repos releases


```sh
pulp publish
```


Congratulations! You have your package published.


- - -


## More Information on Haves


If you don’t know what licence to pick or don’t really care that much, MIT is usually a pretty safe bet.

If your README is markdown it should at least minimally take a form similar to


```md
# purescript-package

Your description.


[![Latest release](http://img.shields.io/github/release/username/purescript-package.svg)](https://github.com/username/purescript-package/releases)
[![Build status](https://travis-ci.org/username/purescript-package.svg?branch=master)](https://travis-ci.org/username/purescript-package)


## Installation

\```sh
bower install purescript-package
\```


Module documentation is [published on Pursuit](http://pursuit.purescript.org/packages/purescript-package).
```


Documentation comments take the form of `-- | Comment`


```haskell
-- | This is a test comment for `unit`
foo ∷ Unit
foo = unit
```

Will publish to Pursuit with the comment “This is a test comment for `unit`” below the definition. This is super useful for new users of your library to read some plain English when your type is hard to follow or you want to provide addition context. It is safer to assume that you need documentation rather than not.
