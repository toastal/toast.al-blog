{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UnicodeSyntax     #-}

module LDJSON where

import           Data.Aeson (string, (.:?), (.=))
import           Data.Aeson as Aeson


jsonLDCompiler ∷ Compiler (Item String)
jsonLDCompiler = do
  Aeson.object
    [ "@context" .= string "http://schema.org"
    , "@type" .= string "BlogPosting"
    , "headline" .= string "title"
    , "alternativeHeadline" .:? string "subtitle"
    , "image" .:? string "image"
    , "keywords" .:? string "tags"
    , "#wordcount" .= string "body"
    , "url" .:? string "url"
    , "publisher" .:? string "publisher"
    , "datePublished" .= string "date"
    , "dateModified" .:? string "date"
    , "description" .:? string "description"
    , "articleBody" .= string "body"
    --, withObject "author"
    ]

--jsonLDCompiler ∷ Compiler (Item String)
--jsonLDCompiler = Aesonobject
