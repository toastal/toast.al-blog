{-# LANGUAGE ApplicativeDo       #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UnicodeSyntax       #-}

module Main where

import           Control.Applicative            (empty)
import           Control.Monad.IO.Class         (liftIO)
import           Data.Foldable                  (fold)
import           Data.Monoid                    ((<>))
import           Hakyll                         (Context (..),
                                                 ContextField (..),
                                                 FeedConfiguration (..),
                                                 Identifier, Item, Pattern,
                                                 Rules, Tags, (.&&.))
import qualified Hakyll                         as H
import qualified Hakyll.Core.Identifier.Pattern as HPattern
import           System.FilePath                (takeBaseName)
import           Text.Pandoc                    (Extension (Ext_literate_haskell),
                                                 WriterOptions,
                                                 writerExtensions)
import qualified Text.Pandoc                    as Pandoc
import qualified Text.Pandoc.Extensions         as Ext

import           Abbreviations                  (abbreviatons)
import           Compilers                      (csonCompiler, htmlminFilter,
                                                 loadingAttrFixer,
                                                 sugarssCompiler)


type Posts =
  [Item String]


main ∷ IO ()
main = H.hakyll $ do
  categories ∷ Tags ← H.buildCategories postsPattern (H.fromCapture "categories/*.html")
  tags ∷ Tags ← H.buildTags postsPattern (H.fromCapture "tags/*.html")
  --recentFivePosts ∷ Posts ← fmap (take 5) . recentFirst =<< loadAll postsPattern

  let taggedCtx ∷ Context String = postCtxTagged categories tags

  -- Templates --
  H.match "templates/*" $
    H.compile H.templateBodyCompiler

  -- Pages --
  H.match "index.md" $ do
    H.route $ H.setExtension "html"
    H.compile $ do
      posts ∷ Posts ← H.recentFirst =<< H.loadAll (postsPattern .&&. H.hasNoVersion)
      let ictx ∷ Context String = indexCtx taggedCtx posts
      H.getResourceBody
        >>= H.applyAsTemplate ictx
        >>= H.renderPandocWith H.defaultHakyllReaderOptions hakyllWriterOptions
        >>= H.loadAndApplyTemplate "templates/supporting.html" H.defaultContext
        >>= H.loadAndApplyTemplate "templates/default.html" ictx
        >>= H.relativizeUrls
        >>= H.withItemBody htmlminFilter
        >>= H.withItemBody loadingAttrFixer

  H.match (H.fromList [ "about.md", "privacy-policy.md" ]) $ do
    H.route $ H.setExtension "html"
    H.compile $ H.pandocCompilerWith H.defaultHakyllReaderOptions hakyllWriterOptions
      >>= H.loadAndApplyTemplate "templates/supporting.html" H.defaultContext
      >>= H.loadAndApplyTemplate "templates/default.html" H.defaultContext
      >>= H.relativizeUrls
      >>= H.withItemBody htmlminFilter
      >>= H.withItemBody loadingAttrFixer

  H.create [ "archive.html" ] $ do
    H.route H.idRoute
    H.compile $ do
      posts ∷ Posts ← H.recentFirst =<< H.loadAll (postsPattern .&&. H.hasNoVersion)
      let actx ∷ Context String = archiveCtx taggedCtx posts
      H.makeItem ""
        >>= H.loadAndApplyTemplate "templates/archive.html" actx
        >>= H.loadAndApplyTemplate "templates/default.html" actx
        >>= H.relativizeUrls
        >>= H.withItemBody htmlminFilter
        >>= H.withItemBody loadingAttrFixer

  -- Posts --
  H.match postsPattern $ do
    H.route $ H.setExtension "html"
    H.compile $ H.pandocCompilerWith H.defaultHakyllReaderOptions hakyllWriterOptions
      >>= H.saveSnapshot "posts"
      >>= H.loadAndApplyTemplate "templates/post.html" taggedCtx
      >>= H.loadAndApplyTemplate "templates/default.html" taggedCtx
      >>= H.relativizeUrls
      >>= H.withItemBody htmlminFilter
      >>= H.withItemBody loadingAttrFixer

  -- TODO: json-ld, then clean up markup
  --H.match postsPattern $ H.version "jsonld" $ do
  --  H.route $ H.setExtension "jsonld"
  --  H.compile $ H.pandocCompiler -- TODO body escape, #wordcount
  --    >>= H.loadAndApplyTemplate "templates/blog-json-ld.cson" taggedCtx
  --    >>= H.withItemBody (H.unixFilter "node_modules/.bin/cson2json" [])

  -- Tags --
  H.tagsRules categories $ \category pattern → do
    H.route H.idRoute
    H.compile $ do
      posts ∷ Posts ← H.recentFirst =<< H.loadAll (pattern .&&. H.hasNoVersion)
      let tctx ∷ Context String = tagCtx category posts
      H.makeItem ""
        >>= H.loadAndApplyTemplate "templates/tag.html" tctx
        >>= H.loadAndApplyTemplate "templates/default.html" tctx
        >>= H.relativizeUrls
        >>= H.withItemBody htmlminFilter

  H.tagsRules tags $ \tag pattern → do
    H.route H.idRoute
    H.compile $ do
      posts ∷ Posts ← H.recentFirst =<< H.loadAll (pattern .&&. H.hasNoVersion)
      let tctx ∷ Context String = tagCtx tag posts
      H.makeItem ""
        >>= H.loadAndApplyTemplate "templates/tag.html" tctx
        >>= H.loadAndApplyTemplate "templates/default.html" tctx
        >>= H.relativizeUrls
        >>= H.withItemBody htmlminFilter

  H.create [ "atom.xml" ] $ do
    H.route H.idRoute
    H.compile $ do
      let fctx ∷ Context String = feedCtx taggedCtx
      --loadAllSnapshots "posts/*" "posts"
      H.loadAll (postsPattern .&&. H.hasNoVersion)
        >>= fmap (take 20) . H.recentFirst
        >>= H.renderAtom feedConf fctx

  H.create [ "rss.xml" ] $ do
    H.route H.idRoute
    H.compile $ do
      let fctx ∷ Context String = feedCtx taggedCtx
      H.loadAll (postsPattern .&&. H.hasNoVersion)
        >>= fmap (take 20) . H.recentFirst
        >>= H.renderRss feedConf fctx

  -- Static Files --
  let copyover = [ "favicon.ico", "fonts/*", "js/*", "js/demos/*" ]

  H.match (HPattern.fromList copyover) $ do
    H.route H.idRoute
    H.compile H.copyFileCompiler

  H.match "netlify/*" $ do
    H.route $ H.gsubRoute "netlify/" (const "")
    H.compile H.copyFileCompiler

  H.match "sss/*.sss" $ do
    H.route $ H.composeRoutes (H.setExtension "css") (H.gsubRoute "sss/" (const "css/"))
    H.compile $ sugarssCompiler

  H.match "contribute.cson" $ do
    H.route $ H.setExtension "json"
    H.compile $ csonCompiler

  H.match "manifest.cson" $ do
    H.route $ H.setExtension "webmanifest"
    H.compile $ csonCompiler


-------------------------------------------------------------------------------


postsPattern ∷ Pattern
postsPattern =
  "posts/*/*"


-------------------------------------------------------------------------------


hakyllWriterOptions ∷ WriterOptions
hakyllWriterOptions = Pandoc.def
    { Pandoc.writerExtensions = Ext.githubMarkdownExtensions
    , Pandoc.writerHighlightStyle = Nothing  -- TODO: Just pygments
    }


feedConf ∷ FeedConfiguration
feedConf = FeedConfiguration
  { H.feedTitle = "toast.al blog"
  , H.feedDescription = "A blog mostly about code, front-end, & functional programming—sometimes about life & travels."
  , H.feedAuthorName = "toastal"
  , H.feedAuthorEmail = "toastal@protonmail.com"
  , H.feedRoot = "https://toast.al"
  }


-------------------------------------------------------------------------------


postCtx ∷  Context String
postCtx =
  fold
    [ H.dateField "date" "%Y %h %d"
    , H.dateField "dateYear" "%Y"
    , H.dateField "dateMonth" "%h"
    , H.dateField "dateDay" "%d"
    , H.urlField "url"
    --, H.mapContext (H.replaceAll ".html" (const ".jsonld") . H.toUrl) $ H.urlField "jsonldurl"
    , H.defaultContext
    ]


postCtxTagged ∷ Tags → Tags → Context String
postCtxTagged categories tags =
  fold
    [ H.tagsField "categories" categories
    , H.tagsField "tags" tags
    , postCtx
    , H.defaultContext
    ]


indexCtx ∷ Context String → Posts → Context String
indexCtx ctx posts =
  fold
    [ H.listField "posts" ctx (return posts)
    , H.constField "title" ""
    --, H.constField "jsonldurl" ""
    , H.defaultContext
    ]


archiveCtx ∷ Context String → Posts → Context String
archiveCtx ctx posts =
  fold
    [ H.listField "posts" ctx (return posts)
    , H.constField "title" "Archives"
    --, H.constField "jsonldurl" ""
    , H.defaultContext
    ]


tagCtx ∷ String → Posts → Context String
tagCtx tag posts =
  fold
    [ H.listField "posts" postCtx (return posts)
    , H.constField "title" $ "Posts tagged “" <> tag <> "”"
    --, H.constField "jsonldurl" ""
    , H.defaultContext
    ]


feedCtx ∷ Context String → Context String
feedCtx ctx =
  fold
    [ Context $ \k _ i → case k of
        "title" →
          H.getMetadataField (H.itemIdentifier i) "title"
            >>= maybe empty (return . StringField . H.escapeHtml)
        _ →
          empty
    , ctx
    , H.defaultContext
    ]
