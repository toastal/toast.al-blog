{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UnicodeSyntax     #-}

module Compilers where

import           Hakyll (Compiler, Item)
import qualified Hakyll as H


csonCompiler ∷ Compiler (Item String)
csonCompiler =
  H.getResourceString
    >>= H.withItemBody (H.unixFilter "node_modules/.bin/cson2json" [])


sugarssCompiler ∷ Compiler (Item String)
sugarssCompiler =
  H.getResourceString
    >>= H.withItemBody (H.unixFilter "node_modules/.bin/postcss" flags)
  where
    flags ∷ [String]
    flags =
      [ "--no-map"
      , "--parser", "sugarss"
      , "--use", "postcss-nested"
      , "--use", "postcss-normalize"
      , "--use", "postcss-flexbugs-fixes"
      , "--use", "autoprefixer"
      , "--autoprefixer.grid", "true"
      , "--autoprefixer.flexbox", "no-2009"
      , "--use", "postcss-csso"
      ]

htmlminFilter ∷ String → Compiler String
htmlminFilter =
  H.unixFilter "node_modules/.bin/htmlmin" []

loadingAttrFixer ∷ String -> Compiler String
loadingAttrFixer =
  H.unixFilter "sed"
    [ "s/data-loading=/loading=/g"
    ]
