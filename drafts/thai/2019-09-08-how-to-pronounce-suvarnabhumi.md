---
title: How to Pronounce ‘Suvarnabhumi’
subtitle: Why Thai Romanization is Such a Mess
tags: thai language, history, linguistics
description: todo
---

Suvarnabhumi, (สุวรรณภูมิ, golden land), the biggest international airport hub in Bangkok and the 21st busiest in the world, is the coming take-off and landing destination for many tourists. Roll up to a cab on your flight back and say “Suvarnabhumi” as it’s spelled on the signs and not a single cab driver will give you this look 🤨, having no clue about where you’re trying to go.

*TL;DR: it’s _su-wahn-na-poom_ (for the geekier IPA [sùʔ.wān.nā.pʰūːm])*

Why is it not _suv-arn-a-boom-i_? Romanization.

## Romanization Briefly

Romanization is the system for converting from a non-Roman (non-Latin) script to a Roman one. Generally the purpose because a lot of folks can read the Roman alphabet due to the prevalence of European languages (and due to colonialization, familiarity), but not others (and I can only assume that in the long ago, the ASCII limitation on computers came into play as well); this style of Romanization is referred to as ‘phonemic’. The other option is ‘transliteration’ where the idea is to map your language’s sound onto the alphabet regardless of how close the sounds are what an unfamiliar reader would expect the sound to be. Granted there’s still a lot of variation in Roman script (e.g. j being a y-like in many languages that aren’t English).

### So How Does Thai Do It?

Thai is weirdly kind of between phonemic and transliteration, by which I mean it tries to be close to what the reader would expect but a lot of things are askew. Also, as Thai itself likes to attempt to preserve the mother tongue’s spelling where possible, this idea can infect Romanization as well.

The two most popular contemporary ways of romanizing are [Royal Thai General System of Transcription](https://en.m.wikipedia.org/wiki/Royal_Thai_General_System_of_Transcription) and [ISO 11940-2](https://en.m.wikipedia.org/wiki/ISO_11940-2) (and there’s charts so check them out)--although they are basically the same. While they sport no accents and stick without the 26 character set, there are the listed critism of not showing vowel length, tone (critical), not differientiating between “จ” and “ฉ, ช, ฌ” with ‘ch’ (despite j being available from an English perspective), and “โ–ะ, โ–” and “เ–าะ, –อ” with ‘o’ (to which I’d suggest ‘aw’ like “law”).

The school I went to used a different system, but the purpose was phonetics and was quite clear for everyone. What I will take a controversial stance on like the school, however is that which <abbr title="International Phonetic Alphabet">IPA</abbr> denotes aspiration with ʰ, pulling this out of superscript position causes _so_ much confusion with readers.

| Thai        | Contemporary | Proposed |
| ----------- | ------------ | -------- |
| ก           | k            | g        |
| ข ฃ ค ฅ ฆ   | kh           | k        |
| จ           | ch           | j        |
| ฉ ช ฌ       | ch           | sh\*     |
| ฏ ต         | t            | dt       |
| ฐ ฑ ฒ ถ ท ธ | th           | t        |
| ป           | p            | bp       |
| ผ พ ภ       | ph           | p        |

\* I’m not really sure why many have even stuck with ‘ch’


###


One example: อ่อนนุช, a popular suburb outside central Bangkok with many foreign residents, is spelled as “On Nut”, “Onnut”, and “Onnuch”. . But what is up with that last option with ‘ch’ at the end? Well, ช, is usually romanized as ‘ch’, but in the Thai language the only _legal_ stop final consants are romanized to p, t, and k, and ช is changed to t sound as an ending, however it’s spelled with ช because of the word’s origin. So while the ‘ch’ ending lets you easily (sorta) translate back to Thai, it’s so heavily obfuscated the pronunciation to where no one will pronounce it correctly. But also of note is that all options will likely not be pronounced with the long ‘u’ sound as it’s spoken as _ɔ̀ɔn-nút_ like “oo” and not _nut_ like _walnut_

Fun side note: Lao due to a spelling reform generally does not hold onto historical spellings and changed the end sounds to be more phonetic.

### Thai Romanization Systems



